jQuery(function () {
    var username = prompt('Введите свое имя', '');
    if(!username) {
        username = 'User' + Math.round(Math.random() * 1e6);
    };

    var socket = io.connect('http://localhost:3000');
    var messages = $("#messages");
    var message_txt = $("#message_text")
    $('.chat .nick').text(username);

    var connected = false;

    socket.emit('add user', username);

    function msg(nick, message) {
        var m = '<div class="msg">' +
                '<span class="user">' + safe(nick) + ':</span> '
                + safe(message) +
                '</div>';
        messages
                .append(m)
                .scrollTop(messages[0].scrollHeight);
    }

    function msg_system(message) {
        var m = '<div class="msg_system">' + safe(message) + '</div>';
        messages
                .append(m)
                .scrollTop(messages[0].scrollHeight);
    }

    socket.on('new message', function (data) {
        msg(data.username, data.message);
        message_txt.focus();
    });

    socket.on('login', function (data) {
        connected = true;
        msg_system('Добро пожаловать в чат');
    });

    socket.on('user joined', function (data) {
        msg_system(data.username + ' присоединился');
    });

    socket.on('user left', function (data) {
        msg_system(data.username + ' вышел');
    });

    socket.on('disconnect', function () {
        connected = false;
        msg_system('Подключение потеряно');
    });

    socket.on('reconnect', function () {
        connected = true;
        msg_system('Подключение восстановлено');
        if (username) {
          socket.emit('add user', username);
        }
    });

    socket.on('reconnect_error', function () {
        msg_system('Попытка подключения не удалась');
    });

    $("#message_btn").click(function () {
        var text = $("#message_text").val();
        if (text && connected) {
            message_txt.val("");
            socket.emit("new message", {message: text, username: username});
        };
    });

    function safe(str) {
        return str.replace(/&/g, '&amp;')
           .replace(/</g, '&lt;')
           .replace(/>/g, '&gt;');
    }
});