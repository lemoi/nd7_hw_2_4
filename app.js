var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

// Routing
app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
    res.sendfile(__dirname + '/index.html');
});

//подписываемся на событие соединения нового клиента
io.sockets.on('connection', function (socket) {
    var addedUser = false;

    //подписываемся на события добавление пользователя
    socket.on('add user', function (username) {
        if (addedUser) return;

        socket.username = username;
        addedUser = true;

        // Подтвердим пользователю подключение
        socket.emit('login', {});

        // Оповестим остальных о новом собеседнике
        socket.broadcast.emit('user joined', {
          username: socket.username,
        });
    });

    //подписываемся на событие message от клиента
    socket.on('new message', function (message) {
        // посылаем сообщения всем пользователям
        io.sockets.emit('new message', message);
    });

    socket.on('disconnect', function () {
        if (addedUser) {
          socket.broadcast.emit('user left', {
            username: socket.username,
          });
        }
    });
});